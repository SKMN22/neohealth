import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class CatsService{
    constructor(private http: HttpClient){
    }

    getPictureRandom():Observable<any>{
        const headerDict = {
            'x-api-key': '8d9bfe94-40ae-4060-9a5b-d404f72dbc2a',
          }
        const requestOpt = {
            headers: new HttpHeaders(headerDict)
        }
        return this.http.get<any>(`https://api.thecatapi.com/v1/images/search`,requestOpt);
    }

    getSpecificPicture(id: string):Observable<any>{
        const headerDict = {
            'x-api-key': '8d9bfe94-40ae-4060-9a5b-d404f72dbc2a',
          }
        const requestOpt = {
            headers: new HttpHeaders(headerDict)
        }
        return this.http.get<any>(`https://api.thecatapi.com/v1/images/${id}`,requestOpt);
    }
}
