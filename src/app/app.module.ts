import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { HttpClientModule } from '@angular/common/http';
import { CatsComponent } from './components/cats/cats.component';
import { TopCatsComponent } from './components/top-cats/top-cats.component';
import { StatsComponent } from './components/stats/stats.component';
import { CatsService } from './services/service';
import { StoreModule } from '@ngrx/store';
import { reducer, PictureEffect } from './components/store';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [
    AppComponent,
    CatsComponent,
    TopCatsComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forFeature('PICTURE_STATE', reducer),
    EffectsModule.forFeature([PictureEffect]),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([])
  ],
  providers: [CatsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
