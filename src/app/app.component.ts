import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PictureState } from './components/store';
import { getCounting } from './components/store/';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'NeoHelthHW';
  public counter: number;
  private counter$: Observable<number>;

  constructor(private store: Store<PictureState>) {
    this.counter$ = this.store.select(getCounting)

  }

  ngOnInit() {
    this.counter$.subscribe(
      count => {
      this.counter = count
        if (localStorage.getItem('pairs') === null) {
          localStorage.setItem('pairs', this.counter.toString())
        }
        else{
          let prevVal = parseInt(localStorage.getItem('pairs')) + 1;
          localStorage.setItem('pairs',prevVal.toString())
        }
      }
    )

  }

}
