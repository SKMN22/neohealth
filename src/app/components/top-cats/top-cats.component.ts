import { Component, OnInit } from '@angular/core';
import { CatsService } from 'src/app/services/service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-top-cats',
  templateUrl: './top-cats.component.html',
  styleUrls: ['./top-cats.component.scss']
})
export class TopCatsComponent implements OnInit {

  public data:any;

  constructor() {
    
   }

  ngOnInit() {
    this.data = this.allStorage();
  }

  private allStorage() {

    let values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        values.push( JSON.parse(localStorage.getItem(keys[i])) );
    }

    return values.sort((a,b) => (a.numberOfVotes > b.numberOfVotes) ? -1: 1);
}

}
