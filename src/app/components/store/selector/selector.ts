import {createSelector} from '@ngrx/store';
import { getPictureServiceReducer, PictureState } from '../reducers';

// export const getPicture = createSelector(
//     getPictureServiceReducer,
//     (state : PictureState) => state.data
// )

// export const getPictureLoading = createSelector(
//     getPictureServiceReducer,
//     (state : PictureState) => state.loading
// )

export const getCounting = createSelector(
    getPictureServiceReducer,
    (state:PictureState) => state.counter
)

export const getAllUrls = createSelector(
    getPictureServiceReducer,
    (state : PictureState) => state.imagesUrl
)