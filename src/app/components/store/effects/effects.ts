import {Injectable} from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { CatsService } from 'src/app/services/service';
import { GET_RANDOM_PICTURE, GetRandomPictureSuccess, GetRandomPictureFailed, CAT_CHOSEN_CHANGE_COUNTER, CatChosenChangeCounterSuccess, CatChosenChangeCounterFailed } from '../actions/';

@Injectable()
export class PictureEffect {
    constructor(private actions$: Actions, private service: CatsService) {
    }
    // @Effect()
    // uploadJson$ = this.actions$.pipe(
    //     ofType(GET_RANDOM_PICTURE),
    //     switchMap(() => {
    //         return this.service.getPictureRandom().pipe(
    //             map((result) => new GetRandomPictureSuccess(result)),
    //             catchError(err => observableOf(new GetRandomPictureFailed(err)))
    //         )
    //     })
    // )

    // @Effect()
    // voteforCat$ = this.actions$.pipe(
    //     ofType(CAT_CHOSEN_CHANGE_COUNTER),
    //     switchMap((payload, url) => {
    //         return this.service.postVoteForCat(payload).pipe(
    //             map(() => new CatChosenChangeCounterSuccess()),
    //             catchError( err => observableOf(new CatChosenChangeCounterFailed(err)))
    //         )
    //     })
    // )
}