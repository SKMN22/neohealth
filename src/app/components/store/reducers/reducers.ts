import { createFeatureSelector } from '@ngrx/store';
import { PictureActions, GET_RANDOM_PICTURE, GET_RANDOM_PICTURE_SUCCESS, GET_RANDOM_PICTURE_FAILED, CAT_CHOSEN_CHANGE_COUNTER, CAT_CHOSEN_CHANGE_COUNTER_SUCCESS, CAT_CHOSEN_CHANGE_COUNTER_FAILED } from '../actions';

export interface PictureState {
    data: any;
    counter: number;
    loading: boolean;
    imagesUrl: any[];
}

export const initPictureState: any = {
    data: {},
    counter: 0,
    imagesUrl: [],
    loading: false
}

export function reducer(state = initPictureState, action: PictureActions) {
    switch (action.type) {
        case GET_RANDOM_PICTURE:
            return { ...state, loading: true }
        case GET_RANDOM_PICTURE_SUCCESS:
            const imagesUrlGet = state.imagesUrl.push(action.payload.url);
            return { ...state, data: action.payload, loading: false, url: imagesUrlGet }
        case GET_RANDOM_PICTURE_FAILED:
            return { ...state, data: action.payload, loading: false }
        case CAT_CHOSEN_CHANGE_COUNTER:
            const counterAdd = 1 + state.counter;
            return { ...state, counter: counterAdd }
        default:
            return { ...state }
    }
}

export const getPictureServiceReducer = createFeatureSelector<PictureState>(
    'PICTURE_STATE'
)