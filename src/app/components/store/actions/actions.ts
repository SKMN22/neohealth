import {Action} from '@ngrx/store';


export const GET_RANDOM_PICTURE = "[TABLE] get random picture";
export const GET_RANDOM_PICTURE_SUCCESS = "[TABLE] get random picture success";
export const GET_RANDOM_PICTURE_FAILED = "[TABLE] get random picture failed";
export const CAT_CHOSEN_CHANGE_COUNTER = "[CATS] change counter cats";
export const CAT_CHOSEN_CHANGE_COUNTER_SUCCESS = "[CATS] change counter cats success";
export const CAT_CHOSEN_CHANGE_COUNTER_FAILED = "[CATS] change counter cats failed";

export class GetRandomPicture{
    readonly type = GET_RANDOM_PICTURE;

    constructor()
    {
    }
}

export class CatChosenChangeCounter{
    readonly type = CAT_CHOSEN_CHANGE_COUNTER;
    constructor(public payload: number){}
}

export class CatChosenChangeCounterSuccess{
    readonly type = CAT_CHOSEN_CHANGE_COUNTER_SUCCESS;
    constructor(){}
}

export class CatChosenChangeCounterFailed{
    readonly type = CAT_CHOSEN_CHANGE_COUNTER_FAILED;
    constructor(public payload: string){}
}

export class GetRandomPictureSuccess{
    readonly type = GET_RANDOM_PICTURE_SUCCESS;
    constructor(public payload: any){}
}

export class GetRandomPictureFailed{
    readonly type = GET_RANDOM_PICTURE_FAILED;
    constructor(public payload: any){}
}

export type PictureActions = 
GetRandomPicture |
GetRandomPictureSuccess |
GetRandomPictureFailed |
CatChosenChangeCounter|
CatChosenChangeCounterSuccess|
CatChosenChangeCounterFailed;