import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { CatsService } from 'src/app/services/service';
import { Store } from '@ngrx/store';
import { PictureState, CatChosenChangeCounter, getAllUrls } from '../store';
import { Vote } from 'src/app/dtos/votedto';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss']
})
export class CatsComponent implements  OnChanges {

  @Input()
  newImg : number;

  public get$ : Observable<any>;
  public loading: boolean = true;
  public favouriteUrls: any[];
  public url:string = "";
  public id:string = "";

  constructor( private service : CatsService, private store : Store<PictureState>) {
   }

  ngOnChanges(changes : SimpleChanges){
    this.loading  = true;
    console.log(this.newImg);
    this.favouriteUrls = Object.keys(localStorage);
    if(this.newImg <= 10)
    this.service.getPictureRandom().subscribe(
      (response:any) =>{
         this.url = response[0].url
         this.id = response[0].id
         this.loading = false;
      }
    );
    else{
      if((Math.random()* 10) + 1 <=5)
      this.service.getPictureRandom().subscribe(
        (response:any) =>{
           this.url = response[0].url
           this.id = response[0].id
           this.loading = false;
        }
      );
      else{
        const randIndex = Math.floor(Math.random()*this.favouriteUrls.length);
        const imgs = this.favouriteUrls[randIndex];
        this.service.getSpecificPicture(imgs).subscribe(
          (response:any) =>{
            this.url = response.url
            this.id = response.id
            this.loading =false;
          }
        )
      }

    }
    
  }

  manageClick(picture,url){
    this.store.dispatch( new CatChosenChangeCounter(this.newImg + 1));
    this.loading = true;
   
    if (localStorage.getItem(picture) === null) {
      let vote =  new Vote();
      vote.id = picture;
      vote.url = url;
      vote.numberOfVotes = 1;
      vote.lastVote = new Date().toLocaleString();
      localStorage.setItem(picture,JSON.stringify(vote));
    }
    else{
      let localStorageObject = JSON.parse(localStorage.getItem(picture));
      localStorageObject.numberOfVotes = localStorageObject.numberOfVotes+1;
      localStorageObject.lastVote = new Date().toLocaleString(); 
      localStorage.setItem(picture,JSON.stringify(localStorageObject));

    }
  }


}
