import { Component, OnInit, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { PictureState, getCounting } from '../store';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit, OnChanges {

  public data: any;
  constructor(private store : Store<PictureState>) { 
    
   }

  ngOnInit() {
    this.data = localStorage.getItem('pairs');
    console.log(this.data)
  }

  ngOnChanges(){
  }

  

}
