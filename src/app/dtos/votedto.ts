export class Vote {
    id:string;
    url:string;
    numberOfVotes:number;
    lastVote:string;
}